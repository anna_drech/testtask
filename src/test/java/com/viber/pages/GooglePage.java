package com.viber.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * There are locators and methods for buttonSearchOnResultPage on Google page
 */
public class GooglePage extends Page {

    @FindBy(xpath = "//input[@id ='lst-ib']")
    public WebElement fieldInputOfHomepage;

    @FindBy(xpath = "//*[@id='hplogo']")
    public WebElement imageGoogleLogo;

    @FindBy(xpath = "//*[@value='Google Search']")
    public WebElement buttonSearchOnHomepage;

    @FindBy(xpath = ".//*[@value='Search']")
    public WebElement buttonSearchOnResultPage;

    @FindBy(xpath = "//div[@id='rso']/div[@class='_NId'][1]//div[@class='rc']//h3")
    public WebElement linkFirstResult;

    @FindBy(xpath = "//*[@id='logo']/img")
    public WebElement imageLogoOnResultPage;

    public GooglePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void typePhraseInSearchField(String text) {
        setElementText(fieldInputOfHomepage, text);
    }

    public void waitForGooglePageLoaded() {
        waitUntilIsLoaded(fieldInputOfHomepage);
    }

    public void clickOnSearchField() {
        clickElement(fieldInputOfHomepage);
    }

    public boolean isOnGooglePage() {
        return exists(fieldInputOfHomepage);
    }

    public void clickSearchButton() {
        clickElement(buttonSearchOnResultPage);
    }

    public void waitForFoundResults() {
        waitUntilIsLoaded(linkFirstResult);
    }

    public boolean isOnSearchResultsPage(){
        return exists(imageLogoOnResultPage);
    }

    public void clickTheFirstLink(){clickElement(linkFirstResult);}

}
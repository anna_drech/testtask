package com.viber.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * There are locators and methods for buttonSearchOnResultPage on Viber Web page
 */
public class ViberPage extends Page {

    @FindBy(xpath = "//*[@id='navbarCollapse']//li/a[contains(text(),'Viber Out')]")
    public WebElement linkViberOut;

    @FindBy(xpath = "//*[@id='content']//li[2]/a")
    public WebElement iconSecondFlag;

    @FindBy(xpath = "//*[@class='country-flag c-flag-r in']")
    public WebElement iconCallToIndiaFlag;

    public ViberPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    public void waitForViberOutPageLoaded(){waitUntilIsLoaded(iconSecondFlag);}

    public void waitForViberPageLoaded(){waitUntilIsLoaded(linkViberOut);}

    public void clickViberOutLink(){clickElement(linkViberOut);}

    public void clickTheSecondIconFlag(){clickElement(iconSecondFlag);}

    public boolean isOnCallPage(){return exists(iconCallToIndiaFlag);}

    public void waitTillCallPageIsLoaded(){waitUntilIsLoaded(iconCallToIndiaFlag);}

    public boolean isOnViberHomePage(){return exists(linkViberOut);}

    public boolean isOnViberOutPage(){return exists(iconSecondFlag);}

}
package com.viber;

import com.google.common.annotations.VisibleForTesting;
import com.viber.pages.GooglePage;
import com.viber.pages.ViberPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Main class for running tests
 */
public class ViberPageTest extends TestNgTestBase {

    public GooglePage googlePage;
    public ViberPage viberPage;

    private String url = "https://www.google.co.il";
    private String searchPhrase = "Viber";

    @BeforeMethod
    public void initPageObjects() {
        googlePage = PageFactory.initElements(driver, GooglePage.class);
        viberPage = PageFactory.initElements(driver, ViberPage.class);
    }

    @Test
    public void searchInGoogle() {
        driver.get(url);
        System.out.println("\n");

        processGoogleHomePage();
        processGoogleResultPage();
        processViberHomePage();
        processViberOutPage();
        processViberCallPage();

        driver.quit();
        System.out.println("============");
        System.out.println("Test Passed");
        System.out.println("============");
    }

    @VisibleForTesting
    protected void processGoogleHomePage() {
        googlePage.waitForGooglePageLoaded();
        Assert.assertTrue(googlePage.isOnGooglePage());
        System.out.println("Google homepage is loaded");

        googlePage.clickOnSearchField();
        googlePage.typePhraseInSearchField(searchPhrase);
        googlePage.clickSearchButton();
    }

    @VisibleForTesting
    protected void processGoogleResultPage() {
        googlePage.waitForFoundResults();
        Assert.assertTrue(googlePage.isOnSearchResultsPage());
        System.out.println("Google results are loaded");

        googlePage.clickTheFirstLink();
    }

    @VisibleForTesting
    protected void processViberHomePage() {
        viberPage.waitForViberPageLoaded();
        Assert.assertTrue(viberPage.isOnViberHomePage());
        System.out.println("Viber homepage is loaded");

        viberPage.clickViberOutLink();
    }

    @VisibleForTesting
    protected void processViberOutPage() {
        viberPage.waitForViberOutPageLoaded();
        Assert.assertTrue(viberPage.isOnViberOutPage());
        System.out.println("Viber-Out page is loaded");

        // Currently the 2nd icon is Indian flag
        viberPage.clickTheSecondIconFlag();
    }

    @VisibleForTesting
    protected void processViberCallPage() {
        viberPage.waitTillCallPageIsLoaded();
        Assert.assertTrue(viberPage.isOnCallPage());

        System.out.println("Viber-Call page is loaded");
    }

}
